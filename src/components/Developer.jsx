import "./style.css";

function PersonCard(props) {
  return (
    <div className="card">
      <h2>Dev: {props.name}</h2>
      <p>Idade: {props.age}</p>
      <p>País: {props.country}</p>
    </div>
  );
}
export default PersonCard;
