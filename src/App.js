import logo from "./logo.svg";
import "./App.css";
import PersonCard from "./components/Developer";

function App() {
  const pessoas = [
    { nome: "Miqueias", idade: 29, nacionalidade: "Brasil" },
    { nome: "Gabriel", idade: 33, nacionalidade: "Portugal" },
    { nome: "Felipe", idade: 20, nacionalidade: "Angola" },
    { nome: "Mariana", idade: 25, nacionalidade: "Brasil" },
    { nome: "Mari", idade: 25, nacionalidade: "Macao" },
  ];

  return (
    <div className="App">
      {pessoas.map((elem, index) => (
        <PersonCard
          name={elem.nome}
          age={elem.idade}
          country={elem.nacionalidade}
          key={index}
        />
      ))}
    </div>
  );
}

export default App;
